
// activemq.cpp.mfc.dlg.cpp : implementation file
//

#include "stdafx.h"
#include "activemq.cpp.mfc.cli.h"
#include "activemq.cpp.mfc.dlg.h"

#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////

CAmqMfcDlg::CAmqMfcDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ACTIVEMQCPPMFCCLI_DIALOG, pParent) {
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAmqMfcDlg::DoDataExchange(CDataExchange* pDX) {
      CDialogEx::DoDataExchange(pDX);
}

/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CAmqMfcDlg, CDialogEx)
	ON_BN_CLICKED(IDC_AMQ_CMP_ORDER_BTN, OnOrderButtonClick)
	ON_BN_CLICKED(IDC_AMQ_PRC_START_BTN, OnStartButtonClick)
	ON_BN_CLICKED(IDC_AMQ_PRC_STOP_BTN , OnStopButtonClick )
	ON_BN_CLICKED(IDC_AMQ_DLG_EXIT_BTN , OnExitButtonClick )
	ON_WM_PAINT  ()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE  ()
	ON_WM_DESTROY()
	ON_LBN_SELCHANGE(IDC_AMQ_JOB_STAT_COMBO, OnJobStatusChanged)
END_MESSAGE_MAP  ()

/////////////////////////////////////////////////////////////////////////////

#pragma region __custom_properties
bool  CAmqMfcDlg::IsCustomButtonEbabled(const ECUSTOMBUTTONS _button_id) const {
	switch (_button_id) {
	case ECUSTOMBUTTONS::eButtonCloseDialog   :
	case ECUSTOMBUTTONS::eButtonCompleteOrder :
	case ECUSTOMBUTTONS::eButtonStartProcess  :
	case ECUSTOMBUTTONS::eButtonStopProcess   : {
		CWnd* p_button = this->GetDlgItem(_button_id);
		if ( p_button )
			return !!p_button->IsWindowEnabled();
		else
			return false;
		} break;
	default:
		return false;
	}
}
void  CAmqMfcDlg::SetCustomButtonEnable(const ECUSTOMBUTTONS _button_id, const bool _b_enable) {
	switch (_button_id) {
	case ECUSTOMBUTTONS::eButtonCloseDialog   :
	case ECUSTOMBUTTONS::eButtonCompleteOrder :
	case ECUSTOMBUTTONS::eButtonStartProcess  :
	case ECUSTOMBUTTONS::eButtonStopProcess   : {
		CWnd* p_button = this->GetDlgItem(_button_id);
		if ( p_button )
			p_button->EnableWindow(static_cast<BOOL>(_b_enable));

		} break;
	}
}
#pragma endregion

/////////////////////////////////////////////////////////////////////////////

#pragma region __indicator_colors

HRESULT  CAmqMfcDlg::SetCanceledColor(const INDICATORCLRS _clr, const TIndState _state) {

	HRESULT hr_ = m_canceled.State(_clr, _state);
	return  hr_;
}

HRESULT  CAmqMfcDlg::SetProgressColor(const INDICATORCLRS _clr, const TIndState _state) {

	HRESULT hr_ = m_progress.State(_clr, _state);
	return  hr_;
}

#pragma endregion

/////////////////////////////////////////////////////////////////////////////

#pragma region _ctrl_notifications

void CAmqMfcDlg::OnJobStatusChanged(void) {

	CWnd* p_ctrl = this->GetDlgItem(IDC_AMQ_JOB_STAT_COMBO);
	if ( p_ctrl == NULL)
		return;

	CComboBox*  p_job_status = static_cast<CComboBox*>(p_ctrl);
	if (NULL != p_job_status) {
		const INT n_selected = p_job_status->GetCurSel();
		if (CB_ERR != n_selected) { // otherwise, no selected item;

			bool b_ = false;
			b_ = !b_;

		}
	}

}

#pragma endregion

/////////////////////////////////////////////////////////////////////////////

#pragma region __custom_handlers

void CAmqMfcDlg::OnOrderButtonClick(void) {}
void CAmqMfcDlg::OnStartButtonClick(void) {}
void CAmqMfcDlg::OnStopButtonClick (void) {}
void CAmqMfcDlg::OnExitButtonClick (void) { CDialogEx::OnCancel(); }

#pragma endregion

/////////////////////////////////////////////////////////////////////////////

void CAmqMfcDlg::OnCancel () { CDialogEx::OnCancel(); m_progress.State(IDR_AMQ_IND_GEEN_IMG, TIndState::e_clr_on);}

void CAmqMfcDlg::OnClose  () { CDialogEx::OnClose();  }

void CAmqMfcDlg::OnDestroy() {
	m_log.Clear  ();
	m_log.Destroy();
	m_progress.Destroy();
	m_canceled.Destroy();
}

BOOL CAmqMfcDlg::OnInitDialog() {
      CDialogEx::OnInitDialog();

	// Set the icon for this dialog. The framework does this automatically
	// when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_log.Create (this, IDC_AMQ_LOG_LIST);

	CComboBox*  p_job_status = static_cast<CComboBox*>(this->GetDlgItem(IDC_AMQ_JOB_STAT_COMBO));
	if (NULL != p_job_status) {
		p_job_status->AddString(_T("IO"));
		p_job_status->AddString(_T("NIO"));
		p_job_status->SetCurSel(CB_ERR);    // no selection;
	}

	HRESULT hr_ = S_OK;

	hr_ = m_progress.Create(IDC_AMQ_IND_ORDR_PROG  , this); if (SUCCEEDED(hr_)) hr_ = this->SetProgressColor(INDICATORCLRS::eClrGrey, TIndState::e_clr_off);
	hr_ = m_canceled.Create(IDC_AMQ_IND_ORDR_CANCEL, this); if (SUCCEEDED(hr_)) hr_ = this->SetCanceledColor(INDICATORCLRS::eClrGrey, TIndState::e_clr_off);

#pragma region __opt_fun_test

#if (1)
	for (int i_ = 0; i_ < 100; i_ ++) {
		CString cs_msg;
		cs_msg.Format(_T("__this_is_a_test_item: %2d"), i_);
		m_log.Insert(cs_msg);
	}
#endif

#pragma endregion

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAmqMfcDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAmqMfcDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

