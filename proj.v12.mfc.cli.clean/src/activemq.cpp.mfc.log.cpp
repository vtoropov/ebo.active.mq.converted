#include "stdafx.h"
#include "activemq.cpp.mfc.log.h"

using namespace amq::mfc;

/////////////////////////////////////////////////////////////////////////////

CLog:: CLog (void) {}

CLog:: CLog (const CWnd& _dlg, const WORD _lst_res_id) {
	CWnd* p_lst = _dlg.GetDlgItem(_lst_res_id);
	if ( p_lst )
	m_list.Attach(p_lst->Detach());
}
CLog::~CLog (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CLog::Append (const CString& _msg) {

	m_list.InsertString(m_list.GetCount(), _msg);

	return S_OK;
}

HRESULT   CLog::Append (LPCTSTR  _msg) {

	m_list.InsertString(m_list.GetCount(), _msg);

	return S_OK;

}

HRESULT   CLog::Clear  (void) {

	if (m_list.GetCount())
		m_list.ResetContent();

	return S_OK;
}

HRESULT   CLog::Create (const CWnd* _dlg, const WORD _lst_res_id) {

	if (NULL == _dlg)
		return E_POINTER;

	CWnd* p_lst = _dlg->GetDlgItem(_lst_res_id);
	if ( p_lst )
		m_list.Attach(p_lst->Detach());
	return S_OK;
}

HRESULT   CLog::Destroy(void) {

	m_list.Detach(); // MFC CWnd traditionally has a problem with internal window handle: it always tries to destroy the window;
	                 // ATL CWindow is much more suetable for such tasks such keeping a handle of a dialog control,
	                 // the dialog procedure takes care about proper destroying of all controls at appropriate time;
	return S_OK;
}

HRESULT   CLog::Insert (const CString& _msg, const INT _n_ndx_before) {

	const INT n_ndx_before = (_n_ndx_before < 0 ? 0 : _n_ndx_before);
	m_list.InsertString(n_ndx_before, _msg);

	return S_OK;
}

HRESULT   CLog::Insert (LPCTSTR  _msg, const INT _n_ndx_before) {

	const INT n_ndx_before = (_n_ndx_before < 0 ? 0 : _n_ndx_before);
	m_list.InsertString(n_ndx_before, _msg);

	return S_OK;
}