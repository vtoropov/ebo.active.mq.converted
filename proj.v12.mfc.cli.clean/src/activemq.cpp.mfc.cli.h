
// activemq-cpp-mfc-cli.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "activemq.cpp.mfc.res.h"		// main symbols


// CAmqMfcCliApp:
// See activemq.cpp.mfc.cli.cpp for the implementation of this class
//

class CAmqMfcCliApp : public CWinApp
{
public:
	CAmqMfcCliApp();

// Overrides
public:
	virtual BOOL InitInstance(void);
	virtual INT  ExitInstance(void);

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAmqMfcCliApp theApp;
