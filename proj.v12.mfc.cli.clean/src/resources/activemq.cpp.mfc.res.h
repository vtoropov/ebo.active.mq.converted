//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by activemq.cpp.mfc.cli.rc
//
#define IDR_MAINFRAME                   101
#define IDD_ACTIVEMQCPPMFCCLI_DIALOG    103

#define IDC_AMQ_LOG_LIST                105
#define IDC_AMQ_CMP_ORDER_BTN           107
#define IDC_AMQ_PRC_START_BTN           109
#define IDC_AMQ_PRC_STOP_BTN            111
#define IDC_AMQ_DLG_EXIT_BTN            113
#define IDC_AMQ_JOB_STAT_COMBO          115
#define IDC_AMQ_IND_ORDR_PROG           117
#define IDC_AMQ_IND_ORDR_CANCEL         119
#define IDR_AMQ_IND_GEEN_IMG            121
#define IDR_AMQ_IND_YELL_IMG            123
#define IDR_AMQ_IND_GREY_IMG            125

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#define _APS_NEXT_COMMAND_VALUE         32771
#endif
#endif
