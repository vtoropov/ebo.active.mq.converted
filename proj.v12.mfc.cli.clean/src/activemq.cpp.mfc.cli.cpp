
// activemq.cpp.mfc.cli.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "activemq.cpp.mfc.cli.h"
#include "activemq.cpp.mfc.dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "tm.ln.custom.gdi.hlp.h"

// CAmqMfcCliApp

BEGIN_MESSAGE_MAP(CAmqMfcCliApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAmqMfcCliApp construction

CAmqMfcCliApp::CAmqMfcCliApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


tm_ln::ctrl::draw::CGdiPlusLib_Guard guard_; // gdi+ loader;

// The one and only CAmqMfcCliApp object
CAmqMfcCliApp theApp;


// CAmqMfcCliApp initialization

BOOL CAmqMfcCliApp::InitInstance()
{
	guard_.Capture();
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("ActiveMqMfcApp"));

	CAmqMfcDlg    dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
	}


#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	// application, rather than start the application's message pump.
	return FALSE;
}

INT  CAmqMfcCliApp::ExitInstance() {
	
	const INT n_res = CWinApp::ExitInstance();

	guard_.Release();

	return n_res;
}