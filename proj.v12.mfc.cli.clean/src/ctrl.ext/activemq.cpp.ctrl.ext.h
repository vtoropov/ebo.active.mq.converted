#pragma once

namespace amq { namespace mfc { namespace ctrl { namespace ext {

		typedef enum {
			e_clr_off = 0,
			e_clr_on  = 1,
		} EINDICATOR_CTRL_STATE;

		typedef EINDICATOR_CTRL_STATE TIndState;


		class CIndicator_Ext {
		protected:
			TIndState   m_ind_state;
			CStatic     m_ind_ctrl ;

		public:
			 CIndicator_Ext  (void);
			~CIndicator_Ext  (void);

		public:
			HRESULT   Create (const WORD _ctrl_res_id, CWnd* _dlg);
			HRESULT   Destroy(void) ;

			const
			TIndState State  (void) const;
			HRESULT   State  (const WORD _img_res_id, const TIndState);
		};


}}}}