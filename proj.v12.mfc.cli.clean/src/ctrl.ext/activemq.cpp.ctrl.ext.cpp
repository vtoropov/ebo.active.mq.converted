#include "stdafx.h"
#include "activemq.cpp.ctrl.ext.h"

using namespace amq::mfc::ctrl::ext;

#include "tm.ln.custom.gdi.hlp.h"

using namespace tm_ln::ctrl::draw;

/////////////////////////////////////////////////////////////////////////////

CIndicator_Ext:: CIndicator_Ext(void) : m_ind_state(TIndState::e_clr_off) {}
CIndicator_Ext::~CIndicator_Ext(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CIndicator_Ext::Create (const WORD _ctrl_res_id, CWnd* _dlg) {
	if (NULL == _dlg)
		return OLE_E_INVALIDHWND;
	if (_ctrl_res_id == 0)
		return E_INVALIDARG;

	CWnd* p_ctrl = _dlg->GetDlgItem(_ctrl_res_id);
	if ( p_ctrl == NULL)
		return __DwordToHresult(ERROR_NOT_FOUND);

	if (FALSE == this->m_ind_ctrl.Attach(p_ctrl->Detach()))
		return __DwordToHresult(::GetLastError());
	else
		return S_OK;
}

HRESULT   CIndicator_Ext::Destroy(void) {
	this->m_ind_ctrl.Detach(); this->m_ind_state = TIndState::e_clr_off;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

const
TIndState CIndicator_Ext::State  (void) const { return m_ind_state; }

HRESULT   CIndicator_Ext::State  (const WORD _img_res_id, const TIndState _state) {
	if (m_ind_ctrl.m_hWnd == NULL)
		return OLE_E_BLANK;
	this->m_ind_state = _state;

	HBITMAP h_bmp = NULL;
	HRESULT hr_ = CGdiPlusPng_Loader().LoadImage(_img_res_id, h_bmp);
	if (SUCCEEDED(hr_)) {
		m_ind_ctrl.SetBitmap(h_bmp);
		::DeleteObject (h_bmp); h_bmp = NULL;
	}
	return hr_;
}