//
// activemq.cpp.mfc.dlg.h : header file
//

#pragma once
#include "activemq.cpp.mfc.log.h"
#include "activemq.cpp.mfc.res.h"
#include "activemq.cpp.ctrl.ext.h"

typedef enum {
	eButtonCompleteOrder = IDC_AMQ_CMP_ORDER_BTN,
	eButtonStartProcess  = IDC_AMQ_PRC_START_BTN,
	eButtonStopProcess   = IDC_AMQ_PRC_STOP_BTN ,
	eButtonCloseDialog   = IDC_AMQ_DLG_EXIT_BTN ,
} ECUSTOMBUTTONS;

typedef enum {
	eClrGrey   = IDR_AMQ_IND_GREY_IMG,
	eClrGreen  = IDR_AMQ_IND_GEEN_IMG,
	eClrOrange = IDR_AMQ_IND_YELL_IMG,
} INDICATORCLRS;


using amq::mfc::ctrl::ext::CIndicator_Ext;
using amq::mfc::ctrl::ext::TIndState;

// CAmqMfcCliDlg dialog
class CAmqMfcDlg : public CDialogEx
{
// Construction
public:
	CAmqMfcDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ACTIVEMQCPPMFCCLI_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	amq::mfc::CLog m_log;
	CIndicator_Ext m_progress;
	CIndicator_Ext m_canceled;

#pragma region __indicator_colors

	HRESULT  SetCanceledColor(const INDICATORCLRS, const TIndState);
	HRESULT  SetProgressColor(const INDICATORCLRS, const TIndState);

#pragma endregion

#pragma region __custom_properties
	bool  IsCustomButtonEbabled(const ECUSTOMBUTTONS _button_id) const;
	void  SetCustomButtonEnable(const ECUSTOMBUTTONS _button_id, const bool _b_enable);
#pragma endregion

#pragma region __custom_handlers
	afx_msg void OnOrderButtonClick(void);
	afx_msg void OnStartButtonClick(void);
	afx_msg void OnStopButtonClick (void);
	afx_msg void OnExitButtonClick (void);
#pragma endregion

#pragma region _ctrl_notifications
	afx_msg void OnJobStatusChanged(void);
#pragma endregion

#pragma region __enternal_handlers
	virtual BOOL    OnInitDialog();
	afx_msg void    OnPaint  ();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void    OnClose  ();
	afx_msg void    OnDestroy();
	virtual void    OnCancel ();
#pragma endregion

	DECLARE_MESSAGE_MAP()
};
