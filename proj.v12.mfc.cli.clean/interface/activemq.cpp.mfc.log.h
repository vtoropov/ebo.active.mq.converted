#pragma once

namespace amq { namespace mfc {

	class CLog {
	protected:
		CListBox  m_list;
	public:
		 CLog (void);
		 CLog (const CWnd& _dlg, const WORD _lst_res_id);
		~CLog (void);

	public:
		HRESULT   Append (const CString& _msg); // adds an item to the end of the list;
		HRESULT   Append (LPCTSTR  _msg);       // adds an item to the end of the list;
		HRESULT   Clear  (void);                // removes all items from the box;
		HRESULT   Create (const CWnd* _dlg, const WORD _lst_res_id);
		HRESULT   Destroy(void);
		HRESULT   Insert (const CString& _msg, const INT _n_ndx_before = 0); // inserts an item to above specified index;
		HRESULT   Insert (LPCTSTR  _msg, const INT _n_ndx_before = 0);       // inserts an item to above specified index;

	};

}}