::
::   Created by Tech_dog (ebontrop@gmail.com) on 30-Sep-2019 at 12:12:12p, UTC+7, Novosibirsk, Tulenina, Monday;
::   This is command file for checking environment variables that are required for building project(s);
::
@echo off
::
::   Input arguments: none;
::

set v_current=%~dp0
set v_pkg_bas=%v_current%packages

::
:: sets SSL include path;
::
set v_ssl_bas=%v_pkg_bas%\openssl-vc141-static-x86_64.1.1.0\build\native
set v_ssl_inc=%v_ssl_bas%\include\
set v_ssl_bin=%v_ssl_bas%\lib\

@echo Sets SSL include path to:
@echo %v_ssl_inc%

call setx /M SSL_INCLUDE %v_ssl_inc%
@echo
@echo Sets SSL binary root path to:
@echo %v_ssl_bin%

call setx /M SSL_BIN %v_ssl_bin%
@echo
::
:: sets APR include path;
::
set v_apr_bas=%v_pkg_bas%\libapr.1.4.6.2\build\native
set v_apr_inc=%v_apr_bas%\include\
set v_apr_bin=%v_apr_bas%\lib\v110\

@echo Sets APR include path to:
@echo %v_apr_inc%

call setx /M APR_INCLUDE %v_apr_inc%
@echo
@echo Sets APR binary root path to:
@echo %v_apr_bin%

call setx /M APR_BIN %v_apr_bin%
@echo
::
:: sets CPP UNIT include path;
::
set v_unt_bas=%v_pkg_bas%\cppunit.1.12.1.4\build\native
set v_unt_inc=%v_unt_bas%\include\
set v_unt_bin=%v_unt_bas%\lib\v120\Win32\

@echo Sets CPP test unit include path to:
@echo %v_unt_inc%

call setx /M UNT_INCLUDE %v_unt_inc%
@echo
@echo Sets CPP test unit binary root path to:
@echo %v_unt_bin%

call setx /M UNT_BIN %v_unt_bin%
@echo

PAUSE