::
::   Created by Tech_dog (ebontrop@gmail.com) on 28-Sep-2019 at 4:42:13p, UTC+7, Novosibirsk, Tulenina, Saturday;
::   This is command file for checking environment variables that are required for building project(s);
::
@echo off
::
::   Input arguments:
::   (%1)  the first is for specifying a bitness of output binary;
::   name  bits;
::         acceptable values are {x64|x86}; default is x86;
::   (%2)  the second argument is for providing compiling mode/configuration;
::   name  mode;
::         acceptable values are {Debug|Release}; default is Debug;
::   (%3)  the third argument is for providing a type of binary linkage;
::         acceptable values are {static|dynamic}; default is dynamic;
::   dynamic linkage is for creating DLL and for generating proxy lib that is required for managing interface compatibilities;
::   static linkage creates a library that becomes a part of the main executable file;
::   
::

set v_bits=%1
set v_mode=%2
set v_link=%3
set v_current=%~dp0

set v_pkg_base=%v_current%packages\openssl-vc141-static-x86_64.1.1.0\build\native

::
:: sets include path;
::
set v_lib_inc=%v_pkg_base%\include\openssl

@echo Sets include path to:
@echo %v_lib_inc%

call setx SSL_INCLUDE "%v_lib_inc%" /m

::
:: composes a path to appropriate library folder;
::
set v_lib_base=%v_pkg_base%\lib
set v_lib_bits=%v_lib_base%\Win32

if (%v_bits% == x64) set v_lib_bits=%v_lib_base%\x64

@echo The path of bitness specified:
@echo %v_lib_bits%

set v_lib_link=%v_lib_bits%\dynamic

if ("dynamic"=="%v_link%")  goto :_set_val

set v_lib_link=%v_lib_bits%\static
set v_lib_mode=%v_lib_bits%\Debug
if (Release==%v_mode%) set v_lib_mode=%v_lib_bits%\Release

@echo The path of mode specified:
@echo %v_lib_mode%
set v_lib_link=%v_lib_mode%

:_set_val
@echo Link path:
@echo %v_lib_link%
::
:: sets appropriate library path;
::
call setx SSL_BIN %v_lib_link%\  /m

@echo  %SSL_BIN%


